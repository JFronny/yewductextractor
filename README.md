# YewDuct Extractor
A fork of [NewPipeExtractor](https://github.com/TeamNewPipe/NewPipeExtractor/) merging several changes from other forks and adding new features.

## License
[![GNU GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)](https://www.gnu.org/licenses/gpl-3.0.en.html)
